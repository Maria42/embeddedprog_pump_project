/*
 * analog.h
 *
 *  Created on: Dec 23, 2020
 *      Author: maria
 */

#ifndef MAIN_ANALOG_H_
#define MAIN_ANALOG_H_

#include <stdint.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#define AD_MAX (4096)
#define AD_MIN (0)

//esp_timer_handle_t oneshot_timer;
//void oneshot_timer_callback(void* arg);


void analog_init();

BaseType_t ws_init();
BaseType_t ws_deinit();

BaseType_t analog_get(uint16_t *result, BaseType_t wait_tick);

BaseType_t flashing_init();


#endif /* MAIN_ANALOG_H_ */
