/*
 * analog.c
 *
 *  Created on: Dec 23, 2020
 *      Author: maria
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "analog.h"
#include "freertos/semphr.h"
#include "esp_err.h"
#include "driver/adc.h"

#include "esp_console.h"
#include "linenoise/linenoise.h"
#include "argtable3/argtable3.h"
#include "regex.h"


#include "esp_http_server.h"
#include <stdint.h>
#include "esp_system.h"
#include "cJSON.h"
#include "freertos/task.h"
#include <string.h>
#include "nvs.h"
#include "nvs_flash.h"



#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE


#include "esp_log.h"

#define CHANNELS (8)
#define CHANNELS1 (2)
#define QUEUE (15)

#define TAG "ANALOG_DEMO"
#define AD_TASK_STACK_SIZE (4096)  
#define AD_TASK_DELAY_MS (100)
#define MAX_CHANNEL (8)

#define MOVING_AVERAGE_QUEUE_SIZE (10)
#define MOVING_HYSTERESIS_DELTA (10)

#define BIT_START (1<<0)
#define BIT_STOP (1<<1)
#define BIT_GROUPS ((BIT_START) | (BIT_STOP))

#define MIN(a,b) ((a)<(b)?(a):(b))
#define MAX(a,b) ((a)>(b)?(a):(b))

#define CMD_AD "ad"
#define ARG_START "start"
#define HINT_START "Start A/D conversion"

#define ARG_STOP "stop"
#define HINT_STOP "Stop A/D conversion"

#define HELP_AD "analog base commands\n"\
	            "Example: \n"\
	            "ad --channel 0 start\n"\
				"ad --channel 1 stop\n"\
				"ad --channel 7 show\n"

#define ARG_SHOW "show"
#define HINT_SHOW "Show A/D statement"

#define ARG_CHANNEL "channel"
#define HINT_CHANNEL "channel index"


#define ARG_SET "set"  
#define HINT_SET "hint for set"

#define ARG_SET1 "set1" 
#define HINT_SET1 "hint for set1"

#define ARG_SET2 "set2"
#define HINT_SET2 "hint for set2"


#define ARG_QUEUE_SIZE "avg queue size"
#define HINT_AVG "avg size" 

#define ARG_DELTA "delta" 
#define HINT_DELTA "delta value" 

#define ARG_GET "get"
#define HINT_GET "hint for get"

#define ARG_ON "on"
#define HINT_ON "Turn on Pump"

#define ARG_OFF "off"
#define HINT_OFF "Turn off Pump"


#define ARG_QSIZE "qsize"
#define HINT_QSIZE "set a qsize parameter"

#define ARG_DSIZE "dsize"
#define HINT_DSIZE "set a dsize parameter"


#define ARG_MIN "min"
#define HINT_MIN "hint for min"

#define ARG_MAXX "max"
#define HINT_MAX "hint for max"

#define DATA_TYPE_INT "<n>"

#define ARG_MON "mon"
#define HINT_MON "hint for min on time"

#define ARG_MOFF "moff"
#define HINT_MOFF "hint for min off time"


#define ARG_HELP "help"

#define HINT_AD "Some hint for ad command"
#define HINT_HELP "Print this help"

#define PUMP_GPIO GPIO_NUM_14  

#define URI_IS_NOT_AVAILABLE "URI is not available"
#define URI_HELLO "/hello"
#define URI_STAT "/stat"
#define URI_CHANNEL_0 "/0"
#define URI_CHANNEL_1 "/1"
#define URI_PUMP "/pump"



#define KEY_PARAM_QSIZE "qs"
#define KEY_PARAM_DSIZE "ds"
#define KEY_PARAM_MIN "mi"
#define KEY_PARAM_MAX "ma"
#define KEY_PARAM_MON "mon"
#define KEY_PARAM_MOFF "mof"



#define KEY_PARAM_WORD "pw"
#define KEY_PARAM_D_WORD "pdw"
#define KEY_PARAM_FLOAT "pf"
#define KEY_PARAM_DOUBLE "pd"
#define KEY_PARAM_STRING "ps"

#define DEF_PARAM_QSIZE ((int8_t)42)
#define DEF_PARAM_DSIZE ((int8_t)42)
#define DEF_PARAM_MIN ((int8_t)42)
#define DEF_PARAM_MAX ((int8_t)42)
#define DEF_PARAM_MON ((int8_t)42)
#define DEF_PARAM_MOFF ((int8_t)42)

#define DEF_PARAM_WORD ((uint16_t)43)
#define DEF_PARAM_D_WORD ((uint32_t)44)
#define DEF_PARAM_FLOAT ((float)42.0)
#define DEF_PARAM_DOUBLE ((float)43.434343)
#define DEF_PARAM_STRING "Zaphod Beeblebrox"

#define PARAM_STRING_MAX_LEN (50)

#define MIN_PARAM_QSIZE ((int8_t)0)
#define MIN_PARAM_DSIZE ((int8_t)0)
#define MIN_PARAM_MIN ((int8_t)0)
#define MIN_PARAM_MAX ((int8_t)0)
#define MIN_PARAM_MON ((int8_t)0)
#define MIN_PARAM_MOFF ((int8_t)0)



#define MIN_PARAM_BYTE2 ((uint8_t)0)

#define MIN_PARAM_WORD ((uint16_t)10)
#define MIN_PARAM_D_WORD ((uint32_t)30)
#define MIN_PARAM_FLOAT ((float)10.0)
#define MIN_PARAM_DOUBLE ((double)16.0)

#define MAX_PARAM_QSIZE ((int8_t)250)
#define MAX_PARAM_DSIZE ((int8_t)250)
#define MAX_PARAM_MIN ((int8_t)250)
#define MAX_PARAM_MAX ((int8_t)250)
#define MAX_PARAM_MON ((int8_t)250)
#define MAX_PARAM_MOFF ((int8_t)250)


#define MAX_PARAM_BYTE2 ((uint8_t)250)

#define MAX_PARAM_WORD ((uint16_t)50000)
#define MAX_PARAM_D_WORD ((uint32_t)1000000)
#define MAX_PARAM_FLOAT ((float)1000.0)
#define MAX_PARAM_DOUBLE ((double)1600000.0)

typedef struct {
	int8_t param_qsize;
	int8_t param_dsize;
	int8_t param_min;
	int8_t param_max;
    int8_t param_mon; 
    int8_t param_moff; 

	uint16_t param_word;
	uint32_t param_d_word;
	float param_float;
	double param_double;
	char *param_string;
	uint8_t valid;
	uint8_t need_save;
} persistent_data_t;

static persistent_data_t persistent_data;


static struct{
	   struct arg_lit *set; 

		struct arg_int *qsize; 
        struct arg_int *dsize; 
        struct arg_int *mon; 
	    struct arg_int *moff; 

	    struct arg_lit *show;


	    struct arg_int *min; 
	    struct arg_int *max; 


	     //For flash
		struct arg_lit *store;
		struct arg_lit *load;
		struct arg_lit *stat;
		struct arg_str *erase;

	   struct arg_lit *help;  
	   struct arg_lit *start;
	   struct arg_lit *stop;
	   struct arg_int *channel; 

	   struct arg_lit *set2; 
	   struct arg_lit *get;

	   struct arg_lit *on; 
	   struct arg_lit *off; 
       struct arg_end *end;
}ad_args;
//static ad_args ad_args_t;

typedef struct{
	uint8_t delta;
	uint8_t hyst_min;
	uint8_t hyst_max;
}moving_hysteresis_t;

typedef struct {
	uint8_t queue[MOVING_AVERAGE_QUEUE_SIZE];
	uint8_t index;
}moving_average_t;



typedef struct{
	uint8_t verified;
	uint8_t d0;
	uint8_t d1;
	uint8_t t0;
	uint8_t t1;
	float tg_alpha;

}temp_verification_t;

static temp_verification_t temp_verification[CHANNELS1];

typedef struct{
	volatile uint8_t running;
	uint32_t conversions;

	uint8_t raw;
	uint32_t normalized;

	moving_average_t moving_average;
	moving_hysteresis_t moving_hysteresis;
}ad_channel_t;





#define LOAD_KEY_NUMBER(handler, key, func, cast, value, def, min, max) 	\
	do {																	\
		esp_err_t err=func((handler), (key), cast &(value));				\
		if (err!=ESP_OK || (value) < (min) || (value) > (max))				\
		(value) = (def);													\
	} while(0)


#define LOAD_KEY_FI(handler, key, func, cast, value, divider, def, min, max) 	\
	do {																		\
		int fi = 0;																\
		esp_err_t err=func((handler), (key), &fi);								\
		double fd=fi/(double)(divider);											\
		(value)=(err==ESP_OK && fd>=(min) && fd<=(max)) ? fd : (def);			\
	} while(0)

#define LOAD_KEY_STR(handler, key, value, def) 							\
	do {																\
		size_t sl;														\
		esp_err_t err=nvs_get_str(handler, key, NULL, &sl);				\
		if (err==ESP_OK && sl>0){										\
			value=malloc(sl);											\
			err= nvs_get_str(handler, key, value, &sl);					\
			if (err!=ESP_OK)											\
				value=strdup(def);										\
		}																\
	} while(0)

static BaseType_t load_params() {
	ESP_LOGD(TAG, "load_params");
	nvs_handle_t nvs_handler;
	esp_err_t err=nvs_open(TAG, NVS_READONLY, &nvs_handler);
	if (err!=ESP_OK) {
		ESP_LOGE(TAG, "Error opening flash: %d", err);
		return pdFAIL;
	}

	LOAD_KEY_NUMBER(nvs_handler, KEY_PARAM_QSIZE, nvs_get_i8, (int8_t *), persistent_data.param_qsize,
			DEF_PARAM_QSIZE, MIN_PARAM_QSIZE, MAX_PARAM_QSIZE);

	LOAD_KEY_NUMBER(nvs_handler, KEY_PARAM_DSIZE, nvs_get_i8, (int8_t *), persistent_data.param_dsize,
			DEF_PARAM_DSIZE, MIN_PARAM_DSIZE, MAX_PARAM_DSIZE);

	LOAD_KEY_NUMBER(nvs_handler, KEY_PARAM_MON, nvs_get_i8, (int8_t *), persistent_data.param_mon,
			DEF_PARAM_MON, MIN_PARAM_MON, MAX_PARAM_MON);

	LOAD_KEY_NUMBER(nvs_handler, KEY_PARAM_MOFF, nvs_get_i8, (int8_t *), persistent_data.param_moff,
			DEF_PARAM_MOFF, MIN_PARAM_MOFF, MAX_PARAM_MOFF);

	

	nvs_close(nvs_handler);
	return pdTRUE;
}

#define STORE_KEY_NUMBER(handler, key, func, value) 				\
	do { 															\
		err=func(handler, key, value);								\
		if (err!=ESP_OK) {											\
			ESP_LOGE(TAG, "cannot store %s, error %d", key, err);	\
			goto error_exit;										\
		}															\
	} while(0)

#define STORE_KEY_STR(handler, key, value)							\
	do {															\
		esp_err_t err=nvs_set_str(handler, key, value);				\
		if (err!=ESP_OK) {											\
			ESP_LOGE(TAG, "cannot store %s err:%d", key, err);		\
			goto error_exit; 										\
		}															\
		free(value);												\
		size_t sl; 													\
		err=nvs_get_str(handler, key, NULL, &sl);					\
		if (err!=ESP_OK || sl==0)									\
			goto error_exit;										\
	} while(0)

static BaseType_t store_params() {
	ESP_LOGD(TAG, "store_params");
	if (!persistent_data.need_save)
		return pdTRUE;

	nvs_handle_t nvs_handler;
	esp_err_t err=nvs_open(TAG, NVS_READONLY, &nvs_handler); 
	if (err!=ESP_OK) {
		ESP_LOGE(TAG, "cannot open flash: %d", err);
		return pdFAIL;
	}

	STORE_KEY_NUMBER(nvs_handler, KEY_PARAM_QSIZE, nvs_set_i8, persistent_data.param_qsize);
	STORE_KEY_NUMBER(nvs_handler, KEY_PARAM_DSIZE, nvs_set_i8, persistent_data.param_dsize);
	STORE_KEY_NUMBER(nvs_handler, KEY_PARAM_MON, nvs_set_i8, persistent_data.param_mon);
	STORE_KEY_NUMBER(nvs_handler, KEY_PARAM_MOFF, nvs_set_i8, persistent_data.param_moff);


	err=nvs_commit(nvs_handler);
	if (err!=ESP_OK) {
		ESP_LOGE(TAG, "Cannot commit, err: %d", err);
		goto error_exit;
	}


	nvs_close(nvs_handler);
	return pdTRUE;

error_exit:
	nvs_close(nvs_handler);
	return pdFAIL;
}

#define HINT_FLASH "set/show/erase/load/store persistent data"
#define HELP_FLASH "flash command"	\
				   " Example:\n"	\
				   " flash --set --qsize 98\n"	\
                   "flash --set --dsize 89\n"   \
				   " flash --show\n"			\
				   " flash --load\n"			\
				   " flash --store\n"

#define ARG_STORE "store"
#define HINT_STORE "store all to flash"

#define ARG_LOAD "load"
#define HINT_LOAD "load all parameters from flash"

#define ARG_STAT "stat"
#define HINT_STAT "state of flash memory"

#define ARG_ERASE "erase"
#define HINT_ERASE "erase all parameters from flash"

#define ARG_BYTE "byte"
#define HINT_BYTE "set a byte parameter"

#define ARG_WORD "word"
#define HINT_WORD "set a word parameter"

#define ARG_D_WORD "dword"
#define HINT_D_WORD "set a dword parameter"

#define ARG_FLOAT "float"
#define HINT_FLOAT "set a float parameter"

#define ARG_DOUBLE "dbl"
#define HINT_DOUBLE "set a double parameter"

#define ARG_STRING "string"
#define HINT_STRING "set a string parameter"

#define ARG_ALL "all"
#define HINT_ALL "erase all flash"

#define DATA_TYPE_FLOAT "<float>"
#define DATA_TYPE_DOUBLE "<double>"
#define DATA_TYPE_STRING "<string>"
#define DATA_TYPE_ERASE "<key|all>"


static void print_flash_stat() {
	ESP_LOGD(TAG, "print_flash_stat ");
	nvs_stats_t nvs_stat;
	nvs_get_stats(TAG, &nvs_stat);
	printf("Count: Used entries:%d, Free entries:%d, all entries:%d\n",
			nvs_stat.used_entries, nvs_stat.free_entries, nvs_stat.total_entries);
}

static void erase_all() {
	ESP_LOGD(TAG, "erase_all ");
	nvs_handle_t handler;
	esp_err_t err=nvs_open(TAG, NVS_READWRITE, &handler);
	if (err!=ESP_OK) {
		ESP_LOGE(TAG, "error opening flash err:%d", err);
		return;
	}

	err=nvs_erase_all(handler);
	if(err!=ESP_OK) {
		ESP_LOGE(TAG, "cannot erase all:%d", err);
		goto exit;
	}

	err=nvs_commit(handler);
	if (err!=ESP_OK) {
		ESP_LOGE(TAG, "cannot commit erase:%d", err);
		goto exit;
	}

	printf("Flash erased successfully\n");

exit:
	nvs_close(handler);
}

static void erase_key(const char *key) {
	ESP_LOGD(TAG, "erase_key : %s", key);
	if (!key)
		return;

	nvs_handle_t handler;
	esp_err_t err=nvs_open(TAG, NVS_READWRITE, &handler);
	if (err!=ESP_OK) {
		ESP_LOGE(TAG, "cannot open flash: %d", err);
		return;
	}

	nvs_iterator_t it=nvs_entry_find(TAG, key, NVS_TYPE_ANY);
	if (!it) {
		ESP_LOGW(TAG, "Cannot find key:%s", key);
		goto exit;
	}

	err=nvs_erase_key(handler, key);
	if (err!=ESP_OK) {

	}

	err=nvs_commit(handler);
	if (err!=ESP_OK) {
		ESP_LOGW(TAG, "cannot commit erase entry:%s, err:%d", key, err);
		goto exit;
	}

	printf("Key %s is erased\n", key);

exit:
	nvs_close(handler);
}




#define MIME_JSON "text/json"

#define BUFFER_SIZE 1024

static httpd_handle_t server1=NULL;

static char buffer[BUFFER_SIZE];
static char buffer2[BUFFER_SIZE];


static TaskHandle_t bg_task_handler;

volatile uint8_t on_flag;
volatile uint8_t off_flag; 

static TaskHandle_t ad_task_handler;
static ad_channel_t ad_channels[CHANNELS1];

uint8_t channel1; 
uint32_t avg_queue_size;
uint32_t delta_size;
uint8_t min_on_time;
uint8_t min_off_time;
uint8_t t0;
uint8_t t1;

static inline int check_channel(uint8_t ch){
	if(ch>=MAX_CHANNEL){
		ESP_LOGE(TAG,"channel maximum value: %d",MAX_CHANNEL);
		return 0;

	}
	return 1;
}

//ch=channel
static void calc_tg_alpha(uint8_t ch){
	   ESP_LOGD(TAG,"Enter calc_tg_alpha channel : %d",ch);
	   if(!check_channel(ch))
	       return;

	   if(temp_verification[ch].d1>temp_verification[ch].d0 && temp_verification[ch].t1>temp_verification[ch].t0){
		   temp_verification[ch].tg_alpha=(temp_verification[ch].t1-temp_verification[ch].t0)/
				   ((float)( temp_verification[ch].d1 -  temp_verification[ch].d0 ));

		   temp_verification[ch].verified=1;
	   }
}

static void verify_set_min(uint8_t channel, float temp){
       ESP_LOGD(TAG,"Enter verify min channel: %d, temp: %f",channel,temp);
       printf("Enter verify min channel: %d, temp: %f\n",channel,temp);
       if(!check_channel(channel))
          return;

       temp_verification[channel].d0= (uint8_t)42; 
       temp_verification[channel].t0=temp;
       calc_tg_alpha(channel);
}


static void verify_set_max(uint8_t channel, float temp){
    ESP_LOGD(TAG,"Enter verify max channel: %d, temp: %f",channel,temp);
    if(!check_channel(channel))
       return;

    temp_verification[channel].d1=(uint8_t)4000; 
    temp_verification[channel].t1=temp;
    calc_tg_alpha(channel);
}

float temperature_get(uint8_t ch, uint8_t dig_input){
	ESP_LOGD(TAG, "Enter temperature get ch: %d, input is %d",ch,dig_input);

    if(!check_channel(ch))
       return 0.0f;

    if(!temp_verification[ch].verified){
    	printf("Channel %d is not verified yet\n", ch);
        return 0.0f;
    }

    //tx=t0+(D0-D1)  dig_input=Dx
    return temp_verification[ch].t0 + (dig_input - temp_verification[ch].d0)*temp_verification[ch].tg_alpha;
}



int64_t time_on;
int64_t time_off;


float tempb;
float tempf;


int first_time_on=1;
int first_time_off=1;

time_t now_on, last_on,now_off,last_off;

bool valid_on;
bool valid_off;

#define SET_PARAM_NUMERIC1(v, persistent, min, max)  	\
	do {												\
		if (v>=min && v<=max) {							\
			persistent=v;								\
			persistent_data.need_save=1;				\
		}												\
	} while(0)


#define SET_PARAM_NUMERIC2(v, persistent, min, max)  	\
	do {												\
		if (v>=min && v<=max) {							\
			persistent=v;								\
			persistent_data.need_save=1;				\
		}												\
	} while(0)




#define SET_PARAM_TEXT(v, persistent, maxlen)		\
		do { 										\
			if (!(v&&*v&&strlen(v)<maxlen))			\
				return 1;							\
			free(persistent);						\
			persistent=strdup(v);					\
			persistent_data.need_save=1;			\
			return 0;								\
		} while(0)




uint8_t mint;
uint8_t maxt;
static int cmd_ad(int argc, char **argv){
	  int nerrors =arg_parse(argc, argv, (void**) &ad_args);

      if(nerrors || argc == 1 || ad_args.help->count>0){
          printf(HELP_AD);
          return 0;
      }

      if(ad_args.channel->count ==0){
    	 printf("You have to set channel with -c or --channel from %d up to %d \n",0,CHANNELS);
         return 1;
      }

      channel1=ad_args.channel->ival[0];

        if(ad_args.set->count>0){
          if(ad_args.qsize->count>0){
              avg_queue_size=(ad_args.qsize->ival[0]);
  		     SET_PARAM_NUMERIC1(ad_args.qsize->ival[0], persistent_data.param_qsize, MIN_PARAM_QSIZE, MAX_PARAM_QSIZE);
             ESP_LOGD(TAG,"Moving avg queue size: %d",ad_args.qsize->ival[0]);
             return 0;
          }

          if(ad_args.dsize->count>0){
         	 delta_size=ad_args.dsize->ival[0];
   		     SET_PARAM_NUMERIC1(ad_args.dsize->ival[0], persistent_data.param_dsize, MIN_PARAM_DSIZE, MAX_PARAM_DSIZE);
             ESP_LOGD(TAG,"Moving hyst delta: %d",delta_size);
             return 0;
          }

          if(ad_args.mon->count>0){
         	 min_on_time=ad_args.mon->ival[0];
             if(min_on_time>250){
               ESP_LOGD(TAG,"Min on time cannot be greater than 250");
               return 1; 
             }
   		     SET_PARAM_NUMERIC1(ad_args.mon->ival[0], persistent_data.param_mon, MIN_PARAM_MON, MAX_PARAM_MON);
             ESP_LOGD(TAG,"Min on time: %d",min_on_time);
             return 0;
          }

          if(ad_args.moff->count>0){
         	 min_off_time=ad_args.moff->ival[0];
             if(min_off_time>250){
               ESP_LOGD(TAG,"Min off time cannot be greater than 250");
               return 1; 
             }
   		     SET_PARAM_NUMERIC1(ad_args.moff->ival[0], persistent_data.param_moff, MIN_PARAM_MOFF, MAX_PARAM_MOFF);
             ESP_LOGD(TAG,"Min off time: %d",min_off_time);
             return 0;
          }



       }



       if(ad_args.set2->count>0){
       	 if(ad_args.min->count>0){
             t0=ad_args.min->ival[0];
       		 verify_set_min(channel1,ad_args.min->ival[0]);
             ESP_LOGD(TAG,"Min(t0): %d",ad_args.min->ival[0]);
       		return 0;
       }

       	if(ad_args.max->count>0){
             t1=ad_args.max->ival[0];
       		 verify_set_max(channel1,ad_args.max->ival[0]);
             ESP_LOGD(TAG,"Max(t1): %d",ad_args.max->ival[0]);
       		return 0;
       	}
       	return 1;
       }
 
      if(ad_args.show->count>0){
    	  printf("Channel is: %d, Moving avg queue size: %d, Hysterisis Delta: %d, min(t0): %d, max(t1): %d, Min on time: %d, Min off time: %d  \n",
    			   channel1, avg_queue_size , delta_size , 
                   t0 , t1, min_on_time, min_off_time);
    	 
    	 if(valid_on == 1){
            printf("Pump is on since %ld\n",last_on);
            return 0;
    	 }

    	 if(valid_off == 1){
            printf("Pump is off since %ld\n",last_off);
            return 0;
    	 }

      	 if(channel1==0){ 
				 printf("Running: %s, Conversion: %d, "
						"Last raw value: %d, Last normalized value: %d\n",
						 ad_channels[0].running? "true" : "false",
						 ad_channels[0].conversions,
						 ad_channels[0].raw,ad_channels[0].normalized);  
				 return 0;                                             
      	 }

      	 if(channel1==1){ 
				 printf("Running: %s, Conversion: %d, "
						"Last raw value: %d, Last normalized value: %d\n",
						 ad_channels[1].running? "true" : "false",
						 ad_channels[1].conversions,
						 ad_channels[1].raw,ad_channels[1].normalized);
				 return 0;
      	 }
         return 1;

      }




  	if (ad_args.store->count>0) {  
  		printf("storing to flash is %s\n", store_params()==pdPASS ? "successfully": "failed");
  		return 0;
  	}

  	if (ad_args.load->count>0) {  
  		printf("loading from flash is %s\n", load_params()==pdPASS ? "successfully": "failed");
  		return 0;
  	}

  	if (ad_args.stat->count>0) { 
  		print_flash_stat();
  		return 0;
  	}

	if (ad_args.erase->count>0) {  
		if (strcasecmp(ARG_ALL, ad_args.erase->sval[0])==0) 
			erase_all();
		else
			erase_key(ad_args.erase->sval[0]);

		return 0;
	}


      if(ad_args.start->count >0 && ad_args.stop->count >0){
         printf("Start or Stop? \n");
         return 1;
      }


      if(ad_args.get->count>0){
         if(channel1==0){ 
           tempb =temperature_get(channel1, ad_channels[0].normalized);
       	   printf("Temperature belonging to channel %d is %f Celsius\n",channel1,temperature_get(0, ad_channels[0].normalized));
       	   return 0;
         }

        if(channel1==1){ //temperature Tf
     	  tempf=temperature_get(channel1, ad_channels[1].normalized);
       	  printf("Temperature belonging to channel %d is %f Celsius\n",channel1,temperature_get(1, ad_channels[1].normalized));
       	  return 0;
        }

        return 1;
      }


      if(ad_args.start->count > 0){
    	 if(channel1==0){
    		 ad_channels[0].running=1; //gpio 32 adc
             ESP_LOGD(TAG,"A/D Conversion started on channel %d",channel1);
			 return 0;
    	 }

    	 if(channel1==1){
    	     ad_channels[1].running=1; //gpio 35 adc
             ESP_LOGD(TAG,"A/D Conversion started on channel %d",channel1);
			 return 0;
      	 }

         return 1;
      }


      if(ad_args.on->count > 0){
    	 time(&now_on);
    	 //first time on
         if(((tempf - tempb) > 5) && first_time_on==1){
         	 time(&last_on);
         	 printf("Turning on the pump\n");
         	 ESP_LOGD(TAG,"Pump on");
         	 gpio_set_level(PUMP_GPIO, 1);  //turn on the pump
         	 valid_on =1;
         	 valid_off =0;
         	 first_time_on++;
         	 return 0;
         }

         //further time on
         else if(((tempf - tempb) > 5) && (now_on-last_off)>min_on_time){
         	 time(&last_on);
             printf("Turning on the pump\n");
         	 gpio_set_level(PUMP_GPIO, 1);  //turn on the pump
         	 valid_on =1;
         	 valid_off =0;
         	 ESP_LOGD(TAG,"Pump on");
         	 return 0;
         }

         else{
             printf("Cannot on until min on time\n");
             return 0;
         }

         return 1;
     }



      if(ad_args.off->count > 0){
     	 time(&now_off);
         if( (now_off-last_on)>min_off_time){
          	 time(&last_off);
         	 printf("Turning off the pump\n");
         	 gpio_set_level(PUMP_GPIO, 0);  //turn off the pump
         	 valid_off =1;
         	 valid_on =0;
         	 ESP_LOGD(TAG,"Pump off");
          	 first_time_off++;
         	 return 0;
         }
         else{
         	printf("Cannot off until the min off time\n");
         	return 0;
         }
         return 1;
      }



      if(ad_args.stop->count > 0){
     	 if(channel1==0){
 			 ad_channels[0].running=0;
 			 printf("A/D Conversion is Stopping in channel %d\n",channel1);
 			 return 0;
     	 }

     	 if(channel1==1){
 			 ad_channels[1].running=0;
 			 printf("A/D Conversion is Stopping in channel %d\n",channel1);
 			 return 0;
     	 }
      }


      return 1;
}




static void register_ad_cmd(){
       ad_args.set=arg_litn("eE", ARG_SET,0,1, HINT_SET);


	   ad_args.qsize=arg_intn("bB", ARG_QSIZE,"<n>",0,QUEUE, HINT_QSIZE);
	   ad_args.dsize=arg_intn("dD", ARG_DSIZE,"<n>",0,15, HINT_DSIZE);
       ad_args.mon=arg_intn("iI", ARG_MON,"<n>",0,1500, HINT_MON);
       ad_args.moff=arg_intn("yY", ARG_MOFF,"<n>",0,1500, HINT_MOFF);

       ad_args.show=arg_lit0("wW", ARG_SHOW, HINT_SHOW);


       ad_args.min=arg_int0("nN", ARG_MIN, DATA_TYPE_INT, HINT_MIN);
       ad_args.max=arg_int0("xX", ARG_MAXX, DATA_TYPE_INT, HINT_MAX);

       ad_args.store=arg_lit0("uU", ARG_STORE, HINT_STORE);
       ad_args.load=arg_lit0("vV", ARG_LOAD, HINT_LOAD);
       ad_args.stat=arg_lit0(NULL, ARG_STAT, HINT_STAT);
       ad_args.erase=arg_str0(NULL, ARG_ERASE, DATA_TYPE_ERASE, HINT_ERASE);

       ad_args.help=arg_lit0("hH", ARG_HELP, HINT_HELP);
       ad_args.set2=arg_litn("rR", ARG_SET2, 0, 1, HINT_SET2); 
       
       ad_args.start=arg_litn("sS", ARG_START, 0, 1, HINT_START);
       ad_args.stop=arg_litn("pP", ARG_STOP, 0, 1, HINT_STOP);
       ad_args.channel=arg_intn("cC", ARG_CHANNEL, "<n>", 0, CHANNELS, HINT_CHANNEL); 

       ad_args.get=arg_litn("gG", ARG_GET, 0, 1, HINT_GET);

       ad_args.on=arg_litn("oO", ARG_ON, 0, 1, HINT_ON);
       ad_args.off=arg_litn("fF", ARG_OFF, 0, 1, HINT_OFF);

       ad_args.end=arg_end(0);

       const esp_console_cmd_t cmd={
    	      .command=CMD_AD,
			  .help=ARG_HELP,
			  .hint=HINT_AD,
			  .func=&cmd_ad
       };

       //register it
       ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}




static uint16_t get_moving_hysteresis(uint16_t input){
	if(channel1==0){
			   if(input>ad_channels[0].moving_hysteresis.hyst_max){
					  ad_channels[0].moving_hysteresis.hyst_max=MIN(ad_channels[0].moving_hysteresis.hyst_max
							  +ad_channels[0].moving_hysteresis.delta,AD_MAX);

					  ad_channels[0].moving_hysteresis.hyst_min=ad_channels[0].moving_hysteresis.hyst_min-
							  ad_channels[0].moving_hysteresis.delta*2;

					  return ad_channels[0].moving_hysteresis.hyst_max;
			   }

			   if(input<ad_channels[0].moving_hysteresis.hyst_min){
					if(ad_channels[0].moving_hysteresis.hyst_min>AD_MIN + ad_channels[0].moving_hysteresis.delta){
						ad_channels[0].moving_hysteresis.hyst_max -= ad_channels[0].moving_hysteresis.delta;
						ad_channels[0].moving_hysteresis.hyst_min -= ad_channels[0].moving_hysteresis.delta;

					}
					return ad_channels[0].moving_hysteresis.hyst_min;

			   }
	}

	if(channel1==1){
			   if(input>ad_channels[1].moving_hysteresis.hyst_max){
					  ad_channels[1].moving_hysteresis.hyst_max=MIN(ad_channels[1].moving_hysteresis.hyst_max
							  +ad_channels[1].moving_hysteresis.delta,AD_MAX);

					  ad_channels[1].moving_hysteresis.hyst_min=ad_channels[1].moving_hysteresis.hyst_min-
							  ad_channels[1].moving_hysteresis.delta*2;

					  return ad_channels[1].moving_hysteresis.hyst_max;
			   }

			   if(input<ad_channels[1].moving_hysteresis.hyst_min){
					if(ad_channels[1].moving_hysteresis.hyst_min>AD_MIN + ad_channels[1].moving_hysteresis.delta){
						ad_channels[1].moving_hysteresis.hyst_max -= ad_channels[1].moving_hysteresis.delta;
						ad_channels[1].moving_hysteresis.hyst_min -= ad_channels[1].moving_hysteresis.delta;

					}
					return ad_channels[1].moving_hysteresis.hyst_min;

			   }
	}

       return input;
}

static uint16_t get_moving_average(uint16_t input){
	  uint32_t avg=0;
	  uint32_t sum=0;

      if(channel1==0){
			   ad_channels[0].moving_average.queue[ad_channels[0].moving_average.index]=input;
			   ad_channels[0].moving_average.index=(ad_channels[0].moving_average.index+1)%avg_queue_size;

			   for(int i=0;i<avg_queue_size;i++)
					 sum +=ad_channels[0].moving_average.queue[i];
			   avg=sum/avg_queue_size;

      }

      if(channel1==1){
			   ad_channels[1].moving_average.queue[ad_channels[1].moving_average.index]=input;
			   ad_channels[1].moving_average.index=(ad_channels[1].moving_average.index+1)%avg_queue_size;

			   for(int i=0;i<avg_queue_size;i++)
					 sum +=ad_channels[1].moving_average.queue[i];
			   avg=sum/avg_queue_size;

      }

      return avg;

}


 void fn_control(void *args){
	ESP_LOGD(TAG, "Enter analog task");
	for(;;){

		if(ad_channels[1].running){
			  ad_channels[1].raw=adc1_get_raw(ADC1_CHANNEL_7); //GPIO35   from temp sensor
			  ad_channels[1].normalized=get_moving_average(get_moving_hysteresis(ad_channels[1].raw));

		}

		if(ad_channels[0].running){
			  ad_channels[0].raw=adc1_get_raw(ADC1_CHANNEL_4); //GPIO32
			  ad_channels[0].normalized=get_moving_average(get_moving_hysteresis(ad_channels[0].raw));

		}


        vTaskDelay(pdMS_TO_TICKS(AD_TASK_DELAY_MS));
	}
}



 static char* get_params_as_ch0_json(char *buffer, size_t buffer_length){
        cJSON *json=cJSON_CreateObject();
        cJSON *entries;

        if(!(entries=cJSON_AddArrayToObject(json, "entries")))
           goto exit;

        cJSON *cd=cJSON_CreateObject();
          if(!(cJSON_AddNumberToObject(cd, "raw", ad_channels[0].raw)))
               goto exit;

     	  if(!(cJSON_AddNumberToObject(cd, "normalized", ad_channels[0].normalized)))
               goto exit;

    	  if(!(cJSON_AddNumberToObject(cd, "temperature_b", tempb)))
               goto exit;

    	  cJSON *v=cJSON_CreateObject();

    	  if(!(cJSON_AddBoolToObject(v, "valid", temp_verification[0].verified=1)))
    		  goto exit;

    	  if(!(cJSON_AddNumberToObject(v, "d0", temp_verification[0].d0)))
    	       goto exit;

    	  if(!(cJSON_AddNumberToObject(v, "d1", temp_verification[0].d1)))
    	       goto exit;

    	  if(!(cJSON_AddNumberToObject(v, "t0", temp_verification[0].t0)))
    	       goto exit;

    	  if(!(cJSON_AddNumberToObject(v, "t1", temp_verification[0].t1)))
      		   goto exit;

    	  if(!(cJSON_AddNumberToObject(v, "tg_alpha", temp_verification[0].tg_alpha)))
    	       goto exit;

    	  cJSON *parent=cJSON_CreateObject();
    	  if(!parent){
    	     goto exit;
    	  }

	     cJSON_AddItemToObject(parent, "channel data", cd);
	     cJSON_AddItemToObject(parent, "Verification", v);
	     cJSON_AddItemToObject(json, "ch0", parent);

    	 cJSON_PrintPreallocated(json, buffer, buffer_length, true);

exit:
    	 cJSON_Delete(json);
    	 return buffer;

 }




 static char* get_params_as_ch1_json( char *buffer, size_t buffer_length){
        cJSON *json=cJSON_CreateObject();
        cJSON *entries;

        if(!(entries=cJSON_AddArrayToObject(json, "entries")))
           goto exit;

        cJSON *cd=cJSON_CreateObject();
          if(!(cJSON_AddNumberToObject(cd, "raw", ad_channels[1].raw)))
               goto exit;

     	  if(!(cJSON_AddNumberToObject(cd, "normalized", ad_channels[1].normalized)))
               goto exit;

    	  if(!(cJSON_AddNumberToObject(cd, "temperature_f", tempf)))
               goto exit;

    	  cJSON *v=cJSON_CreateObject();

    	  if(!(cJSON_AddBoolToObject(v, "valid", temp_verification[1].verified=1)))
    		  goto exit;

    	  if(!(cJSON_AddNumberToObject(v, "d0", temp_verification[1].d0)))
    	       goto exit;

    	  if(!(cJSON_AddNumberToObject(v, "d1", temp_verification[1].d1)))
    	       goto exit;

    	  if(!(cJSON_AddNumberToObject(v, "t0", temp_verification[1].t0)))
    	       goto exit;

    	  if(!(cJSON_AddNumberToObject(v, "t1", temp_verification[1].t1)))
      		   goto exit;

    	  if(!(cJSON_AddNumberToObject(v, "tg_alpha", temp_verification[1].tg_alpha)))
    	       goto exit;

    	  cJSON *parent=cJSON_CreateObject();
    	  if(!parent){
    	     goto exit;
    	  }

	     cJSON_AddItemToObject(parent, "channel data", cd);
	     cJSON_AddItemToObject(parent, "Verification", v);
	     cJSON_AddItemToObject(json, "ch1", parent);

    	 cJSON_PrintPreallocated(json, buffer, buffer_length, true);

exit:
    	 cJSON_Delete(json);
    	 return buffer;

 }


 static char* get_params_as_pump_json( char *buffer, size_t buffer_length){
     cJSON *json=cJSON_CreateObject();
     cJSON *entries;

     if(!(entries=cJSON_AddArrayToObject(json, "entries")))
         goto exit;

     cJSON *cd=cJSON_CreateObject();
     if(valid_on==1){
          if(!(cJSON_AddStringToObject(cd, "Pump state", "pump now on"))) 
             goto exit;
     }

     if(valid_on==0){
          if(!(cJSON_AddStringToObject(cd, "Pump state", "pump now off"))) 
             goto exit;
     }

     if(!(cJSON_AddNumberToObject(cd, "Pump on since", last_on))) 
        goto exit;

     if(!(cJSON_AddNumberToObject(cd, "Pump off since", last_off))) 
        goto exit;

	  cJSON *parent=cJSON_CreateObject();
	  if(!parent){
	     goto exit;
	  }


	  cJSON_AddItemToObject(parent, "pump data", cd);
	  cJSON_AddItemToObject(json, "stats", parent);

 	  cJSON_PrintPreallocated(json, buffer, buffer_length, true);

exit:
 	 cJSON_Delete(json);
 	 return buffer;

 }



 static esp_err_t ch0_handler(httpd_req_t *req){
       if(!req)
          return ESP_ERR_HTTPD_INVALID_REQ;

       httpd_resp_set_type(req, MIME_JSON);
       httpd_resp_send(req, get_params_as_ch0_json(buffer, BUFFER_SIZE), strlen(buffer));
       return ESP_OK;
 }

 static esp_err_t ch1_handler(httpd_req_t *req){
       if(!req)
          return ESP_ERR_HTTPD_INVALID_REQ;

       httpd_resp_set_type(req, MIME_JSON);
       httpd_resp_send(req, get_params_as_ch1_json(buffer2, BUFFER_SIZE), strlen(buffer2));
       return ESP_OK;
 }

 static esp_err_t pump_handler(httpd_req_t *req){
       if(!req)
          return ESP_ERR_HTTPD_INVALID_REQ;

       httpd_resp_set_type(req, MIME_JSON);
       httpd_resp_send(req, get_params_as_pump_json(buffer2, BUFFER_SIZE), strlen(buffer2));
       return ESP_OK;
 }




 static const httpd_uri_t ch0 = {
 		.uri=URI_CHANNEL_0,
 		.method=HTTP_GET,
 		.handler=ch0_handler,
 		.user_ctx=NULL
 };


 static const httpd_uri_t ch1 = {
 		.uri=URI_CHANNEL_1,
 		.method=HTTP_GET,
 		.handler=ch1_handler,
 		.user_ctx=NULL
 };

 static const httpd_uri_t pump = {
 		.uri=URI_PUMP,
 		.method=HTTP_GET,
 		.handler=pump_handler,
 		.user_ctx=NULL
 };


 BaseType_t ws_init(){
      ESP_LOGD(TAG,"ws init");
      bzero(&persistent_data, sizeof(persistent_data));

	    adc1_config_width(ADC_WIDTH_BIT_12);
		adc1_config_channel_atten(ADC_CHANNEL_4, ADC_ATTEN_DB_6);
		adc1_config_channel_atten(ADC_CHANNEL_7, ADC_ATTEN_DB_6); 

		ad_channels[0].moving_hysteresis.delta=delta_size;
		ad_channels[1].moving_hysteresis.delta=delta_size;

		uint16_t raw1=adc1_get_raw(ADC1_CHANNEL_4);
		uint16_t raw2=adc1_get_raw(ADC1_CHANNEL_7);


		ad_channels[0].moving_hysteresis.hyst_min=raw1-delta_size;
		ad_channels[0].moving_hysteresis.hyst_max=raw1+MOVING_HYSTERESIS_DELTA;

		ad_channels[1].moving_hysteresis.hyst_min=raw2-MOVING_HYSTERESIS_DELTA;
		ad_channels[1].moving_hysteresis.hyst_max=raw2+MOVING_HYSTERESIS_DELTA;

		register_ad_cmd();

      httpd_config_t config=HTTPD_DEFAULT_CONFIG();
      ESP_LOGI(TAG,"Starting server on port: %d",config.server_port);

      esp_err_t err1=httpd_start(&server1, &config);


      if(err1==ESP_OK){
     	 ESP_LOGI(TAG,"Registering URI handler");
     	 httpd_register_uri_handler(server1, &ch0);
     	 httpd_register_uri_handler(server1, &ch1);
     	 httpd_register_uri_handler(server1, &pump);
         xTaskCreate(fn_control, "webserver", 4096,NULL, uxTaskPriorityGet(NULL), bg_task_handler);  
         return pdPASS;
      }


      ESP_LOGE(TAG,"Error in starting the server");
      return pdFAIL;
 }




 BaseType_t flashing_init() {
 	bzero(&persistent_data, sizeof(persistent_data));
 	esp_err_t err=load_params();
 	if (err!=pdPASS)
 		return err;

	register_ad_cmd();
 	return pdPASS;
 }



