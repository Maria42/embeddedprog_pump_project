
# Final Embedded Prog 2 Project


## How to use example
-) For temp sensor on GPIO32(ADC1_CHANNEL_4) of esp32
-) You can set the avg queue size with for eg. for channel 0 from flash:               ad -c 0 --set --qsize 10
-) To store the avg queue size to flash:                                               ad -c 0 --store --qsize 
-) Now you have to set the delta size from flash:                                      ad -c 0 --set --dsize 10 
-) To store the delta size to flash:                                                   ad -c 0 --store --dsize
-) Now you can set the t0(Min temp) from command line:                                 ad -c 0 --set2 --min 10
-) Now you can set the t1(Max temp) from command line:                                 ad -c 0 --set2 --max 100
-) Now you can start the ad conversion on channel 0:                                   ad -c 0 --start
-) To see the temp values on channel 0:                                                ad -c 0 --get
-) To see everything:                                                                  ad -c 0 --show
-) You can stop the ad process on channel 0 now:                                       ad -c 0 --stop


-) For temp sensor on GPIO35(ADC1_CHANNEL_7) of esp32
-) Now start the similar process for temp sensor 2 
-) You can set the avg queue size with for eg. for channel 1 from flash:               ad -c 1 --set --qsize 10
-) To store the avg queue size to flash:                                               ad -c 1 --store --qsize 
-) Now you have to set the delta size from flash:                                      ad -c 1 --set --dsize 10 
-) To store the delta size to flash:                                                   ad -c 1 --store --dsize
-) Now you can set the t0(Min temp) from command line:                                 ad -c 1 --set2 --min 10
-) Now you can set the t1(Max temp) from command line:                                 ad -c 1 --set2 --max 100
-) Now you can start the ad conversion on channel 0:                                   ad -c 1 --start
-) To see the temp values on channel 1:                                                ad -c 1 --get
-) To see everything:                                                                  ad -c 1 --show
-) You can stop the ad process on channel 1 now:                                       ad -c 1 --stop

-) To start the pump, first you need to give min on/off times from flash for eg 15:    ad -c 1 --set --mon 15    ad -c 1 --set --moff 15
-) And store them to flash                                                             ad -c 1 --store --mon     ad -c 1 --store --moff
          
-) To turn on the pump:                                                                ad -c 1 --on
-) To turn off the pump:                                                               ad -c 1 --off

-) JSON for ch0:                                                                       http://192.168.1.68/0
-) JSON for ch1:                                                                       http://192.168.1.68/1
-) JSON for pump:                                                                      http://192.168.1.68/pump

-) Pump supposed to be at: GPIO_NUM_14 of ESP332

